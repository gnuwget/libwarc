/*
 * Copyright (c) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libwarc.
 *
 * LibWARC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibWARC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwget.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Header file for libwarc library routines
 */
/// \file

#ifndef LIBWARC_LIBWARC_H
#define LIBWARC_LIBWARC_H

#include <stdio.h>

/**
 * \internal
 * \brief Used to declare exported symbols in the public header
 * see https://www.gnu.org/software/gnulib/manual/html_node/Exported-Symbols-of-Shared-Libraries.html
 */
#if defined BUILDING_LIBWARC && HAVE_VISIBILITY
#    define WARCAPI __attribute__((__visibility__("default")))
#elif defined BUILDING_LIBWARC && defined _MSC_VER && !defined LIBWARC_STATIC
#    define WARCAPI __declspec(dllexport)
#elif defined _MSC_VER && !defined LIBWARC_STATIC
#    define WARCAPI __declspec(dllimport)
#else
#    define WARCAPI
#endif

/// Type of the hashmap
typedef struct hashmap_st hashmap;

/// Type of the CDX record
typedef struct cdx_record cdx_record;

/// Type of the WARC context struct
typedef struct warc_context_struct warc_session;

/**
 * \brief Enum for the two WARC versions
 *
 * You can set one of these values for the `warc_version` member in `warc_options`.
 */
typedef enum {
    WARC_VERSION_1_1 = 0x11, /**< For WARC version 1.1 */
    WARC_VERSION_1_0 = 0x10 /**< For WARC version 1.0 */
} warc_version;

/**
 * \brief The options struct for a WARC session.
 *
 * To be used as `warc_options`.
 * The struct containing the options set for the current WARC session.
 * Some of the options in the struct have a true/false value. In terms of integers,
 * true: 0
 * false: -1
 */
typedef struct warc_options_struct {
    int warc_version; /**< WARC specification version. Default: WARC_VERSION_1_1 */
    char *warc_filename; /**< WARC base filename. This is a mandatory field */
    char *tempdir; /**< Path to temp directory. Default "/tmp/libwarc/" */
    char *cdx_dedup_filename; /**< CDX file to be read for a rerun. Default: NULL */
    char **warcinfo_fields; /**< Custom warcinfo fields. Default: NULL */
    int file_limit; /**< WARC file number limit. Default: 0 (no limit) */
    int cdx; /**< Enable or disable the creation of CDX file. Default: -1 */
    int compression; /**< Enable or disable GZIP compression. Default: -1 */
    int digest; /**< Enable or disable block and payload digests. Default: 0 */
    int log_record; /**< Enable or disable log resource record. Default: 0 */
} warc_options;

WARCAPI extern int (*warc_sha1_from_file)(FILE *, void *);
WARCAPI extern void *(*warc_malloc)(size_t);
WARCAPI extern void (*warc_free)(void *);

WARCAPI warc_session *warc_init(const warc_options *);
WARCAPI void warc_close(warc_session *);
WARCAPI void warc_get_default_options(warc_options *);

#endif /* LIBWARC_LIBWARC_H */
