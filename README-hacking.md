## LibWARC testing structure and procedure for writing new tests:

### Testing structure:
Testing for LibWARC is done through the `Check` testing framework. The hierarchy of the testing mechanism in LibWARC has four levels, arranged from the lowest to the highest:
1. Test
2. Test Case
3. Test Suite
4. SRunner

A test case is a collection of tests and a test suite a collection of test cases. An SRunner is an object that runs a set of test suites.

In terms of LibWARC, testing is divided into distinct files. Each file is its own self-contained C source file which would be compiled into its own executable to be supplied to the `check_PROGRAMS` and `TESTS` variables in `Makefile.am`.

Each test program has a single test suite and an SRunner in its `main` function. Each test suite would contain a set of test cases. Each test case would contain a single test.

### Syntax for a single test:
Each test is written as:
```C
START_TEST(<test-name>)
{
    <test-code>
    <assert-function>
} END_TEST
```

For example, the following test passes if flag obtains a value of 1 in the course of the test code:
```C
START_TEST(test_example)
{
    int flag = 0;
    <test-code>
    ck_assert_int_eq(flag, 1);
} END_TEST
```

### Creating a test suite:
All such tests are consolidated into a test suite with a function that returns a pointer to the created test suite. The function follows this procedure:
1. The test suite is created using `Suite *<test-suite> = suite_create(<test-suite-name>);`
2. The test cases are created using `TCase *<test-case> = tcase_create(<test-case-name>);`
3. Tests are added to test cases using `tcase_add_test(<test-case>, <test-name>);`
4. Test cases are added to the test suite using `suite_add_tcase(<test-suite>, <test-case>);`
5. The pointer `<test-suite>` is returned at the end of the function.

### Procedure to add a new test:
To add a new test, follow these instructions:
1. If the new test case is relevant to an already written test file:
    - The test should be written to that file using the `START_TEST` and `END_TEST` macros.
    - In the test suite creation function of the file, the newly written test should be added to a new test case.
    - The new test case should be added to the current test suite.

2. If the new test case is not relevant to an existing test file:
    - Create a new test file with a main function. The code for this function should be similar to the other test files.
    - Write a function for creating a test suite with the procedure mentioned before in this document.
    - Tests can be written to the file as usual with the procedure in step 1.

The currently written test files in the `tests/` directory have the structure and procedure described here. These can be studied for reference.
