dnl This file is the template file for autoconf.
dnl Copyright (C) 2020 Free Software Foundation, Inc.

dnl libwarc is free software: you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation, either version 3 of the License, or
dnl (at your option) any later version.

dnl libwarc is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.

dnl You should have received a copy of the GNU General Public License
dnl along with libwarc.  If not, see <https://www.gnu.org/licenses/>.

dnl Initial setup
AC_PREREQ([2.64])
LT_PREREQ([2.4.2])
AC_INIT([libWARC],
        [0.1],
        [wget-libwarc@gnu.org],
        [libwarc],
        [https://gitlab.com/gnuwget/libwarc])

dnl What version of LibWARC are we building?
AC_MSG_NOTICE([Configuring for LibWARC $PACKAGE_VERSION])

dnl Configure Directories
AC_CONFIG_SRCDIR([src/libwarc_main.c])
AC_CONFIG_AUX_DIR([build-aux])

dnl Initialize Automake
AM_INIT_AUTOMAKE([subdir-objects gnu check-news])

dnl Prevent autoconf from setting CFLAGS automatically.
CFLAGS="$CFLAGS"

AC_PROG_CC_C99
if test x"$ac_cv_prog_cc_c99" = x"no"; then
	AC_MSG_ERROR([LibWARC needs a C99 compatble compiler. Aborting.])
fi

AC_CANONICAL_BUILD
AC_CANONICAL_HOST

gl_EARLY

AC_CONFIG_MACRO_DIRS([m4])

dnl Initialize libtool
LT_INIT()

AC_PROG_SED
AC_PROG_MKDIR_P

gl_INIT

AC_ARG_ENABLE([tests],
    AS_HELP_STRING([--disable-tests], [Disable running the tests]),
    ,
    enable_tests="yes")

warc_MANYWARNINGS_INIT(WARN_CFLAGS, C)
warc_MANYWARNINGS_CLEAN(WARN_CFLAGS)
warc_MANYWARNINGS_GL(GNULIB_WARN_CFLAGS, WARN_CFLAGS)

if test x"$enable_tests" != x"no"; then
    PKG_CHECK_MODULES([CHECK], [check >= 0.10.0])
fi
AM_CONDITIONAL([ENABLE_TESTS], [test x"$enable_tests" != "xno"])
AC_CHECK_PROGS([DOXYGEN], [doxygen])
AC_CHECK_PROGS([DOT], [dot])
AS_IF([test -z "$DOXYGEN"], [AC_MSG_WARN([Doxygen not found - continuing without Doxygen support])])

AC_ARG_ENABLE([developer-mode],
	      AS_HELP_STRING([--enable-developer-mode], [Enable developer settings.]))

if test x"$enable_developer_mode" = xyes; then
    CFLAGS="-g -ggdb3 -Og $CFLAGS"
else
    CFLAGS="-DNDEBUG -O2 $CFLAGS"
fi

dnl Check for UUID related libraries
dnl Here, we first check for libuuid using PKG_CHECK_MODULES
dnl If not found, we use AC_SEARCH_LIBS
dnl If not found then we search for uuid_create() from uuid.h
dnl If not found we use the fallback mechanism for UUID generation

PKG_CHECK_MODULES([UUID], [uuid],[
    LIBS="$UUID_LIBS $LIBS"
    CFLAGS="$UUID_CFLAGS $CFLAGS"
    AC_DEFINE([HAVE_LIBUUID], [1], [Define if libuuid is available])
], [
    AC_SEARCH_LIBS([uuid_generate], [uuid], [
        AC_DEFINE([HAVE_LIBUUID], [1], [Define if libuuid is available])
    ], [
        AC_CHECK_HEADER([uuid.h], AC_CHECK_FUNCS([uuid_create], [], [
                AC_MSG_WARN([*** libuuid and uuid_create not found. Using fallback mechanism])
        ]), AC_MSG_WARN([*** libuuid and uuid_create not found. Using fallback mechanism]))
    ])
])

dnl Option to disable building examples
AC_ARG_ENABLE([examples],
    AS_HELP_STRING([--disable-examples], [Disable building examples]),
    ,
    enable_examples="yes")
AM_CONDITIONAL([EXAMPLES], [test "x$enable_examples" != "xno"])

AM_CONDITIONAL([HAVE_DOXYGEN],[test -n "$DOXYGEN" && test -n "$DOT"])

AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([Makefile lib/Makefile])
AM_COND_IF([HAVE_DOXYGEN], [AC_CONFIG_FILES([docs/Doxyfile])])

AC_SUBST(WARN_CFLAGS)
AC_SUBST(GNULIB_WARN_CFLAGS)

AC_OUTPUT

AC_MSG_NOTICE([Configuring LibWARC ($build -- $host):
  Version:               ${PACKAGE_VERSION}
  Install Prefix:        ${prefix}
  Compiler:              ${CC}
  CFlags:                ${CPPFLAGS} ${CFLAGS} ${WARN_CFLAGS}
  LDFlags:               ${LDFLAGS}
  Library Types:         Shared=${enable_shared}, static=${enable_static}
  Tests:                 ${enable_tests}
  Examples:              ${enable_examples}
])
