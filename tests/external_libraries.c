/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sha1.h>

#include "libwarc.h"
#include "private.h"
#include "digest.h"
#include "uri.h"

/*
 * This file contains tests for the wrapper functions for
 * external libraries
 *
 * There are three external libraries being tested here:
 * 1. libuuid for the UUID generation for Record ID.
 * 2. Gnulib `sha1` and `base32` modules for digest
 * calculation.
 * 3. zlib for compression.
 *
 * The testing procedures are for testing whether wrapper
 * functions work as expected
 */

/*
 * This test is for the libuuid wrapper functions.
 *
 * The testing procedure is:
 * 1. Call the function `generate_uuid` for generating a UUID
 * string.
 * 2. The string is then checked for valid characters and the
 * valid placement of hyphens.
 */
START_TEST(test_uri)
{
    int flag = 1;

    // 48 is the total length of the UUID string
    char *uuid_str = malloc(48 * sizeof(char));

    // Seeding the random number generators in stdlib
    srandom((unsigned)(time(NULL) ^ getpid()));

    // Setting the memory management functions
    warc_malloc = malloc;
    warc_free = free;
    generate_uuid(uuid_str);

    /*
     * i = 10 accounts for the angular bracket, uuid and
     * urn labels. i = 46 is the last angular bracket hence,
     * i ends at 45. Thus, we are examining only the 36 characters
     * in the UUID.
     */
    for (int i = 10; i < 46; i++) {
        if (!((uuid_str[i] >= 'a' && uuid_str[i] <= 'f') ||
            (uuid_str[i] >= '0' && uuid_str[i] <= '9') ||
            (uuid_str[i] == '-'))) {
                flag = 0;
        }
        if (uuid_str[i] == '-') {
            if (i != 18 && i != 23 && i != 28 && i != 33) {
                flag = 0;
            }
        }
    }

    free(uuid_str);

    ck_assert_int_eq(flag, 1);
} END_TEST

/*
 * This is for testing the digest calculation procedure.
 *
 * The testing procedure is:
 * 1. Data with known block and payload digests is written to a file.
 * 2. The file is then supplied to function `get_digest_headers` to obtain
 * the digests.
 * 3. After calculation, the obtained digests are compared with the correct ones.
 */
START_TEST(test_digests)
{
    int flag = 0;

    FILE *f = fopen("samplefile.txt", "w+");
    if (f == NULL) {
        perror("Couldn't open file");
    }
    fprintf(f, "This is the start of the block\nThis is the payload");

    char *block = malloc((DIGEST_HEADER_SIZE) * sizeof(char));
    char *payload = malloc((DIGEST_HEADER_SIZE) * sizeof(char));

    // For setting the SHA1 function
    warc_sha1_from_file = sha1_stream;

    // For setting the memory management functions
    warc_malloc = malloc;
    warc_free = free;

    // Here, 31 is the payload offset in the data printed in the file
    get_digest_headers(f, 31, block, payload);

    const char *block_correct = "sha1:AVBZMDRZ4RCW42QHOTJHDTO3UVCQ3BZN";
    const char *payload_correct = "sha1:VN52JRZ4WCQ3OAERFFZEFBB7HZ5IHS7M";

    if (STREQ(block, block_correct) && STREQ(payload, payload_correct)) {
        flag = 1;
    }

    fclose(f);
    remove("samplefile.txt");
    free(block);
    free(payload);

    ck_assert_int_eq(flag, 1);
} END_TEST

/*
 * This test suite is for testing functions related to external libraries,
 * which are for UUID generation, SHA1 digest calculation and compression.
 */
static Suite *make_external_libraries_suite(void)
{
    Suite *s;
    TCase *tc_digests, *tc_uri;

    s = suite_create("external_libraries");

    tc_uri = tcase_create("uri");
    tcase_add_test(tc_uri, test_uri);
    suite_add_tcase(s, tc_uri);

    tc_digests = tcase_create("digests");
    tcase_add_test(tc_digests, test_digests);
    suite_add_tcase(s, tc_digests);

    return s;
}

int main(void)
{
    int no_failed = 0;
    SRunner *runner;

    runner = srunner_create(make_external_libraries_suite());

    srunner_set_tap(runner, "libwarc_external_libraries.tap");
    srunner_run_all(runner, CK_NORMAL);
    no_failed = srunner_ntests_failed(runner);
    srunner_free(runner);
    return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
