/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file
 *
 * \brief Contains functions for generating UUID strings
 *
 * There are three methods being used here to generate UUIDs
 * depending on the availability of external libraries:
 * 1. Using libuuid.
 * 2. Using uuid_create() from uuid.h(from linux-libc-headers).
 * 3. A fallback mechanism involving manual generation of UUIDs
 * using random numbers.
 *
 * The last method is followed only if the other two are not possible.
 */

#include <config.h>

#include <stdlib.h>
#include <stdio.h>

#if HAVE_LIBUUID
#    include <uuid/uuid.h>
#elif HAVE_UUID_CREATE
#    include <uuid.h>
#endif

#include "libwarc.h"
#include "uri.h"

/**
 * Uniform Resource Name (URN) for UUIDs are 32-chars long.
 */
#define UUID_URN_LENGTH 32

/**
 * The UUID has 4 hyphens to separate the different parts
 */
#define UUID_DECORATOR 4

/**
 * \brief Size of a UUID string with the labels and angular brackets.
 *
 * This constitutes:
 * 36 characters: The full UUID URN
 *  9 characters: for UUID and URN labels,
 *  2 characters: for angular brackets
 */
#define UUID_MAX_SIZE (UUID_URN_LENGTH + UUID_DECORATOR + 9 + 2)

#if HAVE_LIBUUID
static void generate_uuid_from_libuuid(char *uuid_str)
{
    uuid_t uuid_bin;

    // This will generate 128 bit binary URI stored at memory
    // location pointed to by uuid_bin.
    uuid_generate(uuid_bin);

    // 32 characters for URN, 4 characters for hyphens and
    // 1 byte for the null character
    char *uuid = warc_malloc(UUID_URN_LENGTH + UUID_DECORATOR + 1);

    uuid_unparse_lower(uuid_bin, uuid);

    snprintf(uuid_str, UUID_MAX_SIZE + 1, "<urn:uuid:%s>", uuid);

    warc_free(uuid);
}

#elif HAVE_UUID_CREATE
static void generate_uuid_with_uuid_create(char *uuid_str)
{
    uuid_t uuid_bin;
    char *uuid;

    // This will generate 128 bit binary URI stored at memory
    // location pointed to by uuid_bin.
    uuid_create(&uuid_bin, NULL);

    uuid_to_string(&uuid_bin, &uuid, NULL);

    snprintf(uuid_str, UUID_MAX_SIZE + 1, "<urn:uuid:%s>", uuid);

    // uuid_to_string() allocates a buffer for the UUID string
    warc_free(uuid);
}

#else
// Fallback mechanism in case libuuid and uuid_create are not found
// RFC 4122, a version 4 UUID with only random numbers
static void generate_uuid_fallback(char *uuid_str)
{
    unsigned char uuid_data[16];
    int i;

    for (i = 0; i < 16; i++)
        uuid_data[i] = random() % 255;

    /*
     * Set the four most significant bits (bits 12 through 15) of the
     * time_hi_and_version field to the 4-bit version number
     */
    uuid_data[6] = (uuid_data[6] & 0x0F) | 0x40;

    /*
     * Set the two most significant bits (bits 6 and 7) of the
     * clock_seq_hi_and_reserved to zero and one, respectively.
     */
    uuid_data[8] = (uuid_data[8] & 0xBF) | 0x80;

    snprintf(uuid_str, UUID_MAX_SIZE + 1,
             "<urn:uuid:%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x>",
             uuid_data[0], uuid_data[1], uuid_data[2], uuid_data[3], uuid_data[4], uuid_data[5],
             uuid_data[6], uuid_data[7], uuid_data[8], uuid_data[9], uuid_data[10], uuid_data[11],
             uuid_data[12], uuid_data[13], uuid_data[14], uuid_data[15]);
}
#endif

/**
 * \param[out] uuid_str The string containing the final UUID string
 *
 * \brief Generates a UUID string to be written to a WARC record.
 *
 * The function receives a character pointer as a parameter and writes
 * the required UUID string to that location. This string is ready to be
 * written to a WARC record since it has the required labels(urn:uuid:) and
 * angular brackets.
 *
 * The implementation of uuid generation using random numbers(in the else macro block)
 * is a slightly modified version of Wget's implementation of the same.
 *
 */
void generate_uuid(char *uuid_str)
{
#if HAVE_LIBUUID
    generate_uuid_from_libuuid(uuid_str);
#elif HAVE_UUID_CREATE
    generate_uuid_with_uuid_create(uuid_str);
#else
    generate_uuid_fallback(uuid_str);
#endif
}
