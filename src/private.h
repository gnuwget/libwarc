/*
 * Copyright (c) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libwarc.
 *
 * LibWARC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibWARC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwget.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file
 *
 * \brief Private functions and defines for libwarc sources
 * \private
 */

#ifndef WARC_PRIVATE_H
#define WARC_PRIVATE_H

#include "libwarc.h"

/**
 * Stringify a given argument.
 * This double macro is needed to stringify a macro argument
 */
#define STRINGIFY(a) #a

/**
 * Internal macro for adding a compiler specific pragma to ignore certain warnings
 */
#define WARC_INTERNAL_IGNORE_WARNING_START(compiler, warning) \
    _Pragma(STRINGIFY(compiler diagnostic push))              \
        _Pragma(STRINGIFY(compiler diagnostic ignored warning))

/**
 * Internal macro to stop ignoring a warning
 */
#define WARC_INTERNAL_IGNORE_WARNING_END(compiler) _Pragma(STRINGIFY(compiler diagnostic pop))

#if defined __clang__
#    define WARC_IGNORE_WARNING_START(a) WARC_INTERNAL_IGNORE_WARNING_START(clang, STRINGIFY(a))
#    define WARC_IGNORE_WARNING_END WARC_INTERNAL_IGNORE_WARNING_END(clang)
#elif defined __GNUC__
#    define WARC_IGNORE_WARNING_START(a) WARC_INTERNAL_IGNORE_WARNING_START(GCC, STRINGIFY(a))
#    define WARC_IGNORE_WARNING_END WARC_INTERNAL_IGNORE_WARNING_END(GCC)
#else
/**
 * Temporarily ignore a compiler warning.
 *
 * This is useful when we know that a compiler will emit a false positive in some place.
 * WARNING: Careful when using this macro. It should not be used within any other statement.
 * No care has been taken to prevent errors when not used correctly.
 */
#    define WARC_IGNORE_WARNING_START(a)
/**
 * Stop ignoring compiler warnings.
 */
#    define WARC_IGNORE_WARNING_END
#endif

// String comparison macros
#define STREQ(a, b) (strcmp(a, b) == 0)
#define STRNEQ(a, b) (!STREQ(a, b))

/**
 * \brief The context struct for the current WARC session
 *
 * To be used as `warc_session`.
 *
 * This contains the relevant file pointers and variables for the current WARC
 * session
 */
struct warc_context_struct {
    FILE *current_file; /**< Current WARC file */
    FILE *manifest_file; /**< Manifest file for the current WARC session */
    FILE *cdx_file; /**< The CDX index file for the current session */
    FILE *log_file; /**< The log file for the current session. */
    FILE *config_file; /**< The file containing configuration data for the current WARC session */
    hashmap *dedup_table; /**< The pointer to the CDX dedup hash table */
    int number_of_files; /**< The number of files written in the current WARC session */
    const warc_options *options; /**< The options struct for the current WARC session */
};

// Used to destroy the given WARC session
void destroy_warc_session(warc_session *);

#endif /* WARC_PRIVATE_H */
