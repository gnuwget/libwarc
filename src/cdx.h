/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBWARC_CDX_H
#define LIBWARC_CDX_H

#include <sha1.h>

/**
 * \file
 * \brief Header file for CDX file functions
 */

/**
 * \brief The struct representing a CDX record
 *
 * Here, 22 is the length of a UTC timestamp formatted according to ISO 8601
 */
struct cdx_record {
    char *url; /**< The URL of the record */
    char date[22]; /**< The timestamp for when the record was written */
    char *digest; /**< The payload digest of the record */
    char *response_id; /**< The UUID of the original response record */
};

int start_cdx_file(warc_session *);
int write_cdx_records_dedup(FILE *cdx_dedup_file, warc_session *session);
void free_dedup_table(warc_session *session);

#endif /* LIBWARC_CDX_H */
