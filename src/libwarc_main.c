/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <time.h>
#include <unistd.h>
#include <string.h>

#include <stdlib.h>
#include <sha1.h>

#include "libwarc.h"
#include "file.h"
#include "cdx.h"
#include "private.h"

/**
 * \file
 *
 * \brief The file containing LibWARC general API function implementations
 */

/**
 * \brief Function pointer for the user specified SHA1 function
 *
 * \param[in] FILE* The file pointer to read for input for digest calculation
 * \param[out] void* The pointer to an allocated memory location for storing the binary digest
 *
 * \return 0 If the procedure is successful
 * \return 1 If the procedure is not successful
 *
 * \details This function defaults to `sha1_stream` from gnulib unless set by the user
 *
 * You can access and set this pointer before calling `warc_init` but it can be changed
 * at any point in the session.
 */
int (*warc_sha1_from_file)(FILE *, void *);

/**
 * \brief Function pointer for the user specified memory allocation function
 *
 * \param[in] size_t The size of the memory to allocate
 *
 * \return void* The pointer to the allocated memory location
 *
 * \details This function defaults to stdlib `malloc` unless set by the user
 *
 * You can access and set this pointer before calling `warc_init`. Setting
 * this function after `warc_init` is undefined behaviour.
 */
void *(*warc_malloc)(size_t);

/**
 * \brief Function pointer for the user specified memory deallocation function
 *
 * \param[in] void* The pointer to the allocated memory location
 *
 * \details This function defaults to stdlib `free` unless set by the user
 *
 * You can access and set this pointer before calling `warc_init`. Setting
 * this function after `warc_init` is undefined behaviour.
 */
void (*warc_free)(void *);

/**
 * \param[in] options The option struct supplied by the user
 *
 * \return A pointer to an allocated warc_session structure
 * \return NULL if initialization failed
 * \brief Initialization function for libWARC
 *
 * Performs the following procedures:
 * 1. Seed the `random()` function in stdlib using `srandom()`.
 * 2. Set the default SHA1 function for digests.
 * 3. Set the `warc_options` member in `warc_session`.
 * 4. Call `write_cdx_records_dedup()` for writing the CDX records
 * in a hashtable.
 * 5. Create temporary files for manifest, log and configuration records
 * using `create_temp_file()`.
 * 6. Start a new CDX file if the option is set.
 * 7. Start a new WARC file.
 */
warc_session *warc_init(const warc_options *options)
{
    // Seeding the random number generators in stdlib
    srandom((unsigned)(time(NULL) ^ getpid()));

    /*
     * Setting the default SHA1 function. This can be overriden by
     * setting the function pointer directly. It can be changed
     * at any point throughout a WARC session.
     */
    if (warc_sha1_from_file == NULL) {
        warc_sha1_from_file = sha1_stream;
    }

    warc_session *session;
    session = warc_malloc(sizeof(struct warc_context_struct));
    memset(session, 0, sizeof(struct warc_context_struct));

    // Set the `warc_options` member in `warc_session`
    session->options = options;

    // Read the CDX file for dedup
    if (options->cdx_dedup_filename != NULL) {
        FILE *f = fopen(options->cdx_dedup_filename, "r");
        if (f == NULL) {
            goto err;
        }
        if (write_cdx_records_dedup(f, session) == -1) {
            goto err;
        }
    }

    // Create temporary files for manifest, log and config
    session->manifest_file = create_temp_file(options->tempdir);
    if (session->manifest_file == NULL) {
        goto err;
    }

    if (options->log_record == 0) {
        session->log_file = create_temp_file(options->tempdir);
        if (session->log_file == NULL) {
            goto err;
        }
    }

    session->config_file = create_temp_file(options->tempdir);
    if (session->config_file == NULL) {
        goto err;
    }

    // Start a new CDX index file
    if (options->cdx == 0) {
        if (start_cdx_file(session) == -1) {
            goto err;
        }
    }

    // Start a new WARC file
    session->number_of_files = 0;
    if (start_warc_file(session) == -1) {
        goto err;
    }

    return session;

err:
    destroy_warc_session(session);
    warc_free(session);
    return NULL;
}

/**
 * \param[out] options The struct to be filled with default options
 *
 * \brief Fill the given warc_options struct with default values
 *
 * The memory for the \p options should be allocated before calling this
 * function. Any other data that has been allocated for the members of this
 * struct has to be free'd before freeing the struct.
 *
 * This function has to be called before calling `warc_init`.
 */
void warc_get_default_options(warc_options *options)
{
    /*
     * Setting the default memory allocation function. This should not be
     * changed after `warc_init`.
     */
    if (warc_malloc == NULL) {
        warc_malloc = malloc;
    }

    /*
     * Setting the default memory deallocation function. This should not be
     * changed after `warc_init`.
     */
    if (warc_free == NULL) {
        warc_free = free;
    }

    // Set defaults
    options->warc_version = WARC_VERSION_1_1;

    options->warc_filename = NULL;
    options->tempdir = NULL;
    options->cdx_dedup_filename = NULL;
    options->warcinfo_fields = NULL;

    options->file_limit = 0;
    options->cdx = -1;
    options->compression = -1;
    options->digest = 0;
    options->log_record = 0;
}

/**
 * \param[in] session The context struct for the current WARC session
 *
 * \brief Destroy the given WARC session
 *
 * This function is essentially used to destroy the given WARC session.
 * It closes all the opened files and frees all allocated memory in the
 * WARC context struct.
 *
 * Note that this function does not delete created CDX or WARC files and thus,
 * it is used only internally.
 */
void destroy_warc_session(warc_session *session)
{
    // Destroy the CDX dedup table
    free_dedup_table(session);

    // Close the manifest file
    if (session->manifest_file != NULL) {
        fclose(session->manifest_file);
    }

    // Close the log file
    if (session->log_file != NULL && session->options->log_record == 0) {
        fclose(session->log_file);
    }

    // Close the config file
    if (session->config_file != NULL) {
        fclose(session->config_file);
    }

    // Close the CDX file
    if (session->cdx_file != NULL && session->options->cdx == 0) {
        fclose(session->cdx_file);
    }

    // Close the current WARC file
    if (session->current_file != NULL) {
        fclose(session->current_file);
    }
}

/**
 * Close a WARC session and deinit all internal structures.
 *
 * This function should be the last thing that is called in a WARC session.
 * It will clean up all internal structures and free the \p session passed to
 * the fucntion.
 *
 * \param[in,out] session The session context for the current WARC session
 */
void warc_close(warc_session *session)
{
    destroy_warc_session(session);
    warc_free(session);
}
