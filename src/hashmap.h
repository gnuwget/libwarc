/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBWARC_HASHMAP_H
#define LIBWARC_HASHMAP_H

#include "attribute.h"

/**
 * \file
 * \brief Header file for Hashmap functions
 *
 * This particular file and the corresponding source file `hashmap.c` were taken from libwget
 */

/// Type of the hashmap compare function
typedef int hashmap_compare_fn(const void *key1, const void *key2);

/// Type of the hashmap hash function
typedef unsigned int hashmap_hash_fn(const void *key);

/// Type of the hashmap browse callback function
typedef int hashmap_browse_fn(void *ctx, const void *key, void *value);

/// Type of the hashmap key destructor function
typedef void hashmap_key_destructor(void *key);

/// Type of the hashmap value destructor function
typedef void hashmap_value_destructor(void *value);

hashmap *hashmap_create(int max, hashmap_hash_fn *hash, hashmap_compare_fn *cmp);
void hashmap_set_resize_factor(hashmap *h, float factor);
int hashmap_put(hashmap *h, void *key, void *value);
ATTRIBUTE_PURE int hashmap_size(const hashmap *h);
int hashmap_browse(const hashmap *h, hashmap_browse_fn *browse, void *ctx);
void hashmap_free(hashmap **h);
void hashmap_clear(hashmap *h);
int hashmap_get(const hashmap *h, const void *key, void **value);
#define hashmap_get(a, b, c) hashmap_get((a), (b), (void **)(c))
int hashmap_contains(const hashmap *h, const void *key);
int hashmap_remove(hashmap *h, const void *key);
int hashmap_remove_nofree(hashmap *h, const void *key);
void hashmap_setcmpfunc(hashmap *h, hashmap_compare_fn *cmp);
int hashmap_sethashfunc(hashmap *h, hashmap_hash_fn *hash);
void hashmap_set_key_destructor(hashmap *h, hashmap_key_destructor *destructor);
void hashmap_set_value_destructor(hashmap *h, hashmap_value_destructor *destructor);
void hashmap_set_load_factor(hashmap *h, float factor);

/// Type of the hashmap iterator
typedef struct hashmap_iterator_st hashmap_iterator;

hashmap_iterator *hashmap_iterator_alloc(hashmap *h);
void hashmap_iterator_free(hashmap_iterator **iter);
void *hashmap_iterator_next(hashmap_iterator *iter, void **value);

#endif /* LIBWARC_HASHMAP_H */
