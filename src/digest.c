/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <string.h>

#include <stdlib.h>
#include <sha1.h>
#include <stdbool.h>
#include <base32.h>

#include "libwarc.h"
#include "private.h"
#include "digest.h"

/**
 * \file
 *
 * \brief Contains functions for calculating SHA1 digests
 *
 * The main function here is get_digest_headers(). It should
 * be called for obtaining block and payload digests. In turn, it calls
 * the other functions in this file to generate the final SHA1 base32 digests.
 *
 * You can override the default SHA1 function by setting your
 * own function using `warc_sha1_from_file`.
 */

/**
 * \brief The size for a string containing a Base32 encoded SHA1 digest
 */
#define SHA1_BASE32_SIZE 33

// Static function definitions
static int calculate_digests_from_file(FILE *f, void *block, void *payload,
                                       const long payload_offset);
static void convert_to_base32(const void *bin_digest, char *b32_digest);

/**
 * \param[in] digest1 The first digest to compare
 * \param[in] digest2 The second digest to compare
 *
 * \return 0 if digest1 and digest2 are the same
 * \return -1 if \p digest1 and \p digest2 are not the same
 *
 * \brief Compare two digests
 *
 * This function is useful in comparing digests of response records
 * during reruns of the crawl, for writing revisit records.
 */
int compare_digest(const void *digest1, const void *digest2)
{
    if (memcmp(digest1, digest2, SHA1_DIGEST_SIZE) == 0) {
        return 0;
    } else {
        return -1;
    }
}

/**
 * \param[in] key The SHA1 digest which acts as the key for the hashmap
 *
 * \return hash The hash value of \p key
 *
 * \brief Calculate the hash for the hashmap key
 *
 * This function returns the first few bytes of the key(the SHA1 digest)
 * which can be used as the hash for the hashmap for that particular key
 */
unsigned int hash_sha1_digest(const void *key)
{
    unsigned int hash = 0;
    memcpy(&hash, key, sizeof(unsigned int));
    return hash;
}

/**
 * \param[in] f The file stream to calculate the digest of
 * \param[in] payload_offset The offset for the record payload in the file
 * \param[out] block The block digest in Base32
 * \param[out] payload The payload digest in Base32
 *
 * \return 0 If the calculation of digests was successful
 * \return -1 If the calculation of digests was not successful
 *
 * \brief Prepare digest headers for a record
 *
 * This function would prepare block and payload digests for the record block
 * contained in \p f and provide them to the calling function.
 */
int get_digest_headers(FILE *f, const long payload_offset, char *block, char *payload)
{
    /* Compiler warnings are disabled here since `alloca` is being used.
     * Compilers sometimes display a warning mentioning that `alloca` is a dangerous
     * function.
     * In this case however, it is safe to use since the memory being allocated is fairly
     * small and the pointer to the memory location is only being passed to two static functions.
     */
    WARC_IGNORE_WARNING_START(-Walloca)
    void *block_bin_digest = alloca(SHA1_DIGEST_SIZE);
    void *payload_bin_digest = alloca(SHA1_DIGEST_SIZE);
    WARC_IGNORE_WARNING_END

    int status =
        calculate_digests_from_file(f, block_bin_digest, payload_bin_digest, payload_offset);
    if (status == -1) {
        return -1;
    }

    convert_to_base32(block_bin_digest, block);
    convert_to_base32(payload_bin_digest, payload);

    return 0;
}

/**
 * \param[in] f The file stream to calculate the digest of
 * \param[in] payload_offset The offset for the record payload in the file
 * \param[out] block Void pointer pointing to the binary block digest
 * \param[out] payload Void pointer pointing to the binary block digest
 *
 * \return 0 If the calculation of binary digests was successful
 * \return -1 If the calculation of binary digests was not successful
 *
 * \brief Calculate binary block and payload digests from a file stream
 *
 * This function would calculate binary block and payload digests using the function
 * supplied by you or the `sha1_stream` function in the crypto/sha1 module of gnulib
 * if you have not supplied a function.
 */
static int calculate_digests_from_file(FILE *f, void *block, void *payload,
                                       const long payload_offset)
{
    // Block Digest
    fseek(f, 0, SEEK_SET);
    if (warc_sha1_from_file(f, block) != 0) {
        return -1;
    }

    // Payload Digest
    fseek(f, payload_offset, SEEK_SET);
    if (warc_sha1_from_file(f, payload) != 0) {
        return -1;
    }

    return 0;
}

/**
 * \param[in] bin_digest The binary digest to be converted to Base32
 * \param[out] b32_digest The pointer to the Base32 digest
 *
 * \brief Convert binary SHA1 digest to Base32
 *
 * This is a simple wrapper around the `base32_encode` function from the
 * base32 module of gnulib.
 */
static void convert_to_base32(const void *bin_digest, char *b32_digest)
{
    /* Compiler warnings are disabled here since `alloca` is being used.
     * Compilers sometimes display a warning mentioning that `alloca` is a dangerous
     * function.
     *
     * Here, however, the amount of memory being allocated is small and the pointer doesn't
     * leave this function. This `alloca` is safe to use in this situation.
     */
    WARC_IGNORE_WARNING_START(-Walloca)
    char *temp_digest = alloca((SHA1_BASE32_SIZE) * sizeof(char));
    WARC_IGNORE_WARNING_END

    base32_encode(bin_digest, SHA1_DIGEST_SIZE, temp_digest, SHA1_BASE32_SIZE);
    snprintf(b32_digest, DIGEST_HEADER_SIZE, "sha1:%s", temp_digest);
}

/**
 * \param[in] b32_digest The SHA1 digest in Base32
 * \param[out] bin_digest The SHA1 digest in binary
 *
 * \return -1 If decode is not successful
 * \return 0 If decode is successful
 *
 * \brief Decodes a given Base32 digest into binary
 */
int decode_base32_digest(const char *b32_digest, char *bin_digest)
{
    size_t digest_len = SHA1_DIGEST_SIZE;
    bool ok = base32_decode(b32_digest, strlen(b32_digest), bin_digest, &digest_len);
    if (!ok) {
        return -1;
    }
    return 0;
}
