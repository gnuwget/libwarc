/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "libwarc.h"

/*
 * This example is for the initialization of LibWARC
 *
 * This is the most basic way of using this function, that is,
 * without CDX deduplication and without enabling CDX writing.
 *
 * It simply creates a new WARC file and writes a Warcinfo record.
 */
int main(void)
{
    warc_options *options = malloc(sizeof(warc_options));
    warc_get_default_options(options);
    options->warc_filename = strdup("example_session");

    warc_session *session = warc_init(options);
    assert(session != NULL);
    warc_close(session);

    free(options->warc_filename);
    free(options);
    return 0;
}
